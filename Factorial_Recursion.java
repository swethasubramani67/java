package demo.com;

import java.util.Scanner;

public class Factorial_Recursion {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int input = scan.nextInt();
		
		int fact = factorial(input);
		
		System.out.println(input + " Factorial is: "+fact);
	}

	public static int factorial(int num) {
		
		if(num ==0)
			return 1;
		
		return(num*factorial(num-1));
	}

}

