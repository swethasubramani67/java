package demo.com;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		String input = scan.nextLine();
		
		String reverse = "";
		
		for(int i=input.length()-1;i>=0;i--)
		{
			reverse =  reverse + input.charAt(i);
			
			
		}
		if(input.toLowerCase().equals(reverse))
			System.out.println(input +" is a Palindrome");
		else
			System.out.println(input +" is not a Palindrome");
		
	}

}

