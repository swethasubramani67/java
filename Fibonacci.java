package demo.com;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		
		int first = 0, second = 1;
		
		int third ;
		
		for(int i=0;i<input;i++)
		{
			System.out.println(first+" ");
			
			third = first+second;
			first = second;
			second = third;
		}
	}

}

