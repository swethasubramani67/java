package demo.com;

import java.util.Scanner;

public class SwapWithTwo {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int First = scan.nextInt();
		int Second = scan.nextInt();
		
		First = First+Second;  
		Second = First-Second;  
		First = First-Second;  
		
		System.out.println("First value is "+First);
		System.out.println("Second value is "+Second);
	}

}

