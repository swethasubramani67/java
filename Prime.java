package demo.com;

import java.util.Scanner;

public class Prime {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int input = scan.nextInt();
		
		boolean flag = false;
		
		for(int i=2;i<=input/2;++i)
		{
			if(input%i==0)
			{
				flag = true;
				break;
			}
			
		}
		if(!flag)
			System.out.println(input +" is a prime number");
		else
			System.out.println(input +" is not a prime number");
	}

}

