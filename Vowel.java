package demo.com;

import java.util.Scanner;

public class Vowel {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		String str = scan.nextLine();
		
		System.out.println(str.toLowerCase().matches(".*[aeiou].*"));
		
	}

}

