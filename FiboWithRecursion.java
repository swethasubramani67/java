package demo.com;

import java.util.Scanner;

public class FiboWithRecursion {

	public static void main(String[] args) {
		
         // With Recursion
		
		Scanner scan  = new Scanner(System.in);
				int input = scan.nextInt();
				
				for(int i=0;i<input;i++)
				{
					System.out.println(fibo(i)+" ");
				}
			}
			
			
			public static int fibo(int value) {
				
				if(value<=1)
				    return value;
				return fibo(value-1)+fibo(value-2);  
			}
}
			
			
			
			

	



