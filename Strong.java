import java.util.Scanner;

public class Strong {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		int input  = scan.nextInt();
	 
		// input value to a temporary variable
		
		int temp = input;

		int sum = 0;
		
		 while(temp!=0) {
			 
		int digit =temp%10;

		int i=1,fact = 1;
		
		while(i<=digit)
		{
			fact = fact*i;
			i++;
		}
		sum = sum+fact;
       temp/=10;
       
	  }
		 if(sum == input)
	          System.out.println(input +" is a Strong Number");
	       else
	           System.out.println(input +" is not a Strong Number");

	}

}

