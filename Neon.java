import java.util.Scanner;

public class Neon {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		  System.out.println("Enter a value");
		
		int input = scan.nextInt();
		
		int square = input*input;
	    
	    int div = square/10;
	
	    int result = div+square%10;
	    
        if(result == input)
	    	  System.out.println("Neon Number");
	    else
	    	  System.out.println("Not Neon");
				
	}

}

